# Currency Rates

A sample app that periodically displays currency rates



![Sample Screenshot](currency-rates-demo.gif)

## How To Build The App

This is a Gradle based project. You can build it using Android Studio or from the command line. To 
do so follow the steps below:

1. Install the following software:
   
       - Android SDK:
         http://developer.android.com/sdk/index.html
       - Gradle:
         http://www.gradle.org/downloads
       - Android Studio - Optional: 
         http://developer.android.com/sdk/installing/studio.html

2. Run the Android SDK Manager by pressing the SDK Manager toolbar button
   in Android Studio or by running the `android` command in a terminal
   window.

3. In the Android SDK Manager, ensure that the following are installed,
   and are updated to the latest available version:
   
       - Tools -> Android SDK Platform-tools (latest stable release)
       - Tools -> Android SDK Tools (latest stable release)
       - Tools -> Android SDK Build-tools version (latest stable release)
       - Android 6.0 -> SDK Platform (API 23) at least
       - Android P (API 28)

4. Create a file in the root of the project called local.properties. Enter the path to your Android SDK.
    Eg. `sdk.dir=/opt/android-studio/sdk`

5. Import the project in Android Studio:
   
   1. Press File > Import Project
   2. Navigate to and choose the `settings.gradle.kts` file in this project
   3. Press OK

6. Choose Build > Make Project in Android Studio or run the following
    command in the project root directory: Make sure you've an Android device connected to the computer.
   
    `./gradlew clean build` Assemble the apk and run all checks.
   
   `./gradlew clean test` Execute all the junit test.

## Features

- List of rates that refreshes every 1 second.  
- Swapping of base currency from the list.  
- Retry mechanism when rate refresh fails.  
- Empty view.

## Technical decisions

There were couple of decisions I had to make when starting this project. I have broken them down into two parts. Architecture, which is how I wanted to organize the code. And dependencies, to relegate some of the complexities to third-party libraries.

### Architecture

This being a small project I decided to use the Redux architecture pattern to organize the code in a clean, testable and maintainable manner and also to demonstrate modern app development.

**Pros:**

- Clear separation of concerns.
  - UI
    -  All UI related code are placed there.
  - ViewModel
    - Mediate between the UI and the Model. Handles configuration changes due to view model
  - Model
    - Handles all the business logic and state management.
  - Data
    - All data access code lives here.
- Testability
  - It's easier to test because the UI is accessed via an interface. A lot of the Android components are isolated so it's easier to writes unit test for them.
- Code reuse
  - Repetitive code are easily avoided.
- Loose coupling
  - The ViewModel being an intermediary between the UI code and the model, allows each to evolve independently of each other.

**Cons:**

 It leads to writing a lot of codes and may add some complexities.

### Dependencies

I was contemplating on writing most of the code using standard libraries that comes with Android but I realized, this usually leads to writing unnecessary code and increases the level of complexities and maintenance codst. I decided to leverage some well known and matured libraries to off load these work to.

### Moshi

Used Moshi to handle all the de/serialization of the JSON data. This is a lightweight and highly customizable library for serializing and deserialization JSON data. It eleminates the need to write a lot of boilerplate code when manipulating JSON data set. Hence speeds up development effort.

- [moshi](https://github.com/square/moshi)  

### Retrofit2

Used Retrofit2 as the REST Client for communicating with the server. It's a fast and efficient REST library for the Android platform. It has abstracted a lot of the boilerplate's way of consuming APIs from a server. Hence, this saves a lot of development efforts. It has handle a lot of edge cases I would otherwise been not able to cover.

- [OkHttp](http://square.github.io/retrofit/)

### Databinding

Used Databinding for binding Views and it's methods. This also removes the need to write repetitive codes when you want to maninpulate a view. This makes me write clean and less code. 

[Databinding](https://developer.android.com/topic/libraries/data-binding/)

## Android Jepack

Comes with the ViewModel component for creating lifecycle-aware reactive apps for handling device configuration well. These days are recommended for developing modern Android apps.

[Android Jetpack](https://developer.android.com/jetpack)

# RxJava

To make the app more reactive and leverage a lot on its operators for manipulating fetched data.

[RxJava](https://github.com/ReactiveX/RxJava)

## Recommendations

- Add more tests cases. That way we can easily handle regressions and increase robustness.
- Locally cache the fetched data to a local store. Right now the app only displays data when there is data connection on the device. Locally caching the data will only fetch data from the internet on a need bases.
- Add checkstyle task to the build process to enforce coding style consistencies. This will make it easier to read and follow the code.




















