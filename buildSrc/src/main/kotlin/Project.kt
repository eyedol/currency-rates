object Project {
    const val APPLICATION_ID = "com.addhen.currency"
    const val SOURCE_COMPATIBILITY = "1.8"
    const val COMPILE_SDK = 28
    const val MIN_SDK = 23
    const val TARGET_SDK = 28
    const val VERSION_NAME = "1.0.0"
    const val VERSION_CODE = 1
}
