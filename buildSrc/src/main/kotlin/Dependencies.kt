object Dependencies {

    val timber = "com.jakewharton.timber:timber:4.7.1"

    object GradlePlugin {
        val version = "3.4.0"
        val android = "com.android.tools.build:gradle:$version"
        val r8 = "com.android.tools:r8:1.3.52"
        val kotlin = Kotlin.version
        val navigationSafeArgs =
            "androidx.navigation:navigation-safe-args-gradle-plugin:${AndroidX.Navigation.version}"
        val jetifier = "com.android.tools.build.jetifier:jetifier-processor:1.0.0-beta02"
    }

    object Kotlin {
        val version = "1.3.50"
        val stdlibJvm = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$version"
    }

    object AndroidX {
        val appCompat = "androidx.appcompat:appcompat:1.0.0"
        val recyclerView = "androidx.recyclerview:recyclerview:1.0.0"
        val constraint = "androidx.constraintlayout:constraintlayout:2.0.0-beta2"
        val design = "com.google.android.material:material:1.1.0-beta01"
        val coreKtx = "androidx.core:core-ktx:1.0.1"

        object Lifecycle {
            val version = "2.1.0"
            val extensions = "androidx.lifecycle:lifecycle-extensions:$version"
            val liveData = "androidx.lifecycle:lifecycle-livedata:$version"
            val runtime = "androidx.lifecycle:lifecycle-runtime:$version"
            val compiler = "androidx.lifecycle:lifecycle-compiler:$version"
            val viewModelKtx = "androidx.lifecycle:lifecycle-viewmodel-ktx:$version"
        }

        object Navigation {
            val version = "2.1.0"
            val runtime = "androidx.navigation:navigation-runtime-ktx:$version"
            val runtimeKtx = "androidx.navigation:navigation-runtime-ktx:$version"
            val ui = "androidx.navigation:navigation-ui:$version"
            val fragmentKtx = "androidx.navigation:navigation-fragment-ktx:$version"
            val uiKtx = "androidx.navigation:navigation-ui-ktx:$version"
        }
    }

    object Dagger {
        val version = "2.24"
        val core = "com.google.dagger:dagger:$version"
        val android = "com.google.dagger:dagger-android:$version"
        val support = "com.google.dagger:dagger-android-support:$version"
        val compiler = "com.google.dagger:dagger-compiler:$version"
        val processor = "com.google.dagger:dagger-android-processor:$version"
    }

    object Databinding {
        val compiler = "androidx.databinding:databinding-compiler:${GradlePlugin.version}"
    }

    object RxJava2 {
        val core = "io.reactivex.rxjava2:rxjava:2.2.13"
        val android = "io.reactivex.rxjava2:rxandroid:2.1.1"
        val kotlin = "io.reactivex.rxjava2:rxkotlin:2.4.0"
    }

    object Square {
        val version = "1.8.0"
        val moshi = "com.squareup.moshi:moshi-adapters:$version"

        object Retrofit {
            val version = "2.6.1"
            val core = "com.squareup.retrofit2:retrofit:$version"
            val moshi = "com.squareup.retrofit2:converter-moshi:$version"
            val rxJava = "com.squareup.retrofit2:adapter-rxjava2:$version"
        }

        object OkHttp {
            val version = "4.2.1"
            val core = "com.squareup.okhttp3:okhttp:$version"
            val loggingInterceptor = "com.squareup.okhttp3:logging-interceptor:$version"
        }
    }

    object Test {
        val junit = "junit:junit:4.12"
        val mockitoKotlin = "com.nhaarman.mockitokotlin2:mockito-kotlin:2.1.0"
        val mockitoInline = "org.mockito:mockito-inline:2.27.0"
        val archCoreTesting = "android.arch.core:core-testing:1.1.1"
    }
}
