/*
 * MIT License
 *
 * Copyright (c) 2017 - 2019 Henry Addo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.addhen.currency.presentation.rate.view

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.addhen.currency.RxTestSchedulerRule
import com.addhen.currency.TestAppSchedulers
import com.addhen.currency.com.addhen.currency.data.model.RateTestUtil.createRate
import com.addhen.currency.data.model.Rate
import com.addhen.currency.data.repository.rate.RateRepository
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertFalse
import junit.framework.TestCase.assertTrue
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.util.concurrent.TimeUnit

@RunWith(MockitoJUnitRunner::class)
class RateViewModelTest {

    @Mock
    private lateinit var repository: RateRepository
    private lateinit var viewModel: RateViewModel

    @get:Rule
    val instantRule = InstantTaskExecutorRule()
    @get:Rule
    val testSchedulerRule = RxTestSchedulerRule

    private val testAppScheduler = TestAppSchedulers

    @Before
    fun setUp() {
        viewModel = RateViewModel(schedulers = testAppScheduler, repository = repository)
    }

    @After
    fun clear() {
        viewModel.compositeDisposable.clear()
    }

    @Test
    fun `onAction - should successfully refresh rates`() {
        val expectedRate = createRate()
        ArrangeBuilder().withCurrencyDataRepository(listOf(expectedRate))

        viewModel.onAction(RateAction.Initialize())

        testSchedulerRule.testScheduler.advanceTimeBy(1, TimeUnit.SECONDS)
        viewModel.viewState.observeForever {
            assertEquals(1, it.rates.size)
            assertEquals(expectedRate, it.rates.first())
            assertFalse(it.isLoading)
            assertFalse(it.isEmptyViewShown)
        }
    }

    @Test
    fun `onAction - should successfully refresh rates but with no rates available`() {
        ArrangeBuilder().withCurrencyDataRepository(emptyList())

        viewModel.onAction(RateAction.Initialize())

        testSchedulerRule.testScheduler.advanceTimeBy(1, TimeUnit.SECONDS)
        viewModel.viewState.observeForever {
            assertEquals(0, it.rates.size)
            assertTrue(it.isEmptyViewShown)
            assertFalse(it.isLoading)
        }
    }

    @Test
    fun `onAction - should fail refresh rates`() {
        ArrangeBuilder().withCurrencyDataRepositoryException()

        viewModel.onAction(RateAction.Initialize())

        testSchedulerRule.testScheduler.advanceTimeBy(1, TimeUnit.SECONDS)
        viewModel.viewState.observeForever {
            assertEquals(0, it.rates.size)
            assertTrue(it.isEmptyViewShown)
            assertFalse(it.isLoading)
        }
    }

    @Test
    fun `onAction - should successfully convertAmount`() {
        val expectedRate = createRate()
        val expectedRateTwo = createRate(currency = Rate.Currency.BGN, amount = "3.82")
        val expectedRates = listOf(expectedRate, expectedRateTwo)
        ArrangeBuilder()
            .withCurrencyDataRepository(expectedRates)
            .withConvertRates(expectedRates)

        viewModel.onAction(
            RateAction.Convert(
                baseCurrencyCode = Rate.Currency.AUD.name,
                amount = 0.0,
                rates = listOf(expectedRate)
            )
        )

        viewModel.viewState.observeForever {
            assertEquals(2, it.rates.size)
            assertFalse(it.isEmptyViewShown)
            assertFalse(it.isLoading)
        }
    }

    private inner class ArrangeBuilder {

        fun withCurrencyDataRepository(rates: List<Rate>) = apply {
            whenever(repository.refreshRates(any())).thenReturn(Single.just(rates))
        }

        fun withCurrencyDataRepositoryException() = apply {
            whenever(repository.refreshRates(any())).thenReturn(Single.error(Exception()))
        }

        fun withConvertRates(rates: List<Rate>) = apply {
            whenever(repository.convertAmount(any(), any(), any(), any())).thenReturn(rates)
        }
    }
}