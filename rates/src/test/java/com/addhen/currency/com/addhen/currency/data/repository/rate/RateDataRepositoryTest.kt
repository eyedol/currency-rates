/*
 * MIT License
 *
 * Copyright (c) 2017 - 2019 Henry Addo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.addhen.currency.com.addhen.currency.data.repository.rate

import com.addhen.currency.com.addhen.currency.data.api.RatesDtoTestUtil.createRateDto
import com.addhen.currency.com.addhen.currency.data.model.RateTestUtil.createRate
import com.addhen.currency.data.AppRateHandler
import com.addhen.currency.data.api.RatesDto
import com.addhen.currency.data.model.Rate
import com.addhen.currency.data.repository.rate.RateDataRepository
import com.addhen.currency.data.repository.rate.RateRemoteDataSource
import com.addhen.currency.data.repository.rate.RateRepository
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response

@RunWith(MockitoJUnitRunner::class)
class RateDataRepositoryTest {

    @Mock
    private lateinit var remote: RateRemoteDataSource
    private lateinit var rateDataRepository: RateRepository

    @Before
    fun setUp() {
        rateDataRepository = RateDataRepository(remote, AppRateHandler())
    }

    @Test
    fun `refreshRates - should successfully refresh rate`() {
        val ratesDto = createRateDto(
            hashMapOf(
                Rate.Currency.BGN.name to 1.9616,
                Rate.Currency.AUD.name to 1.6212
            )
        )
        ArrangeBuilder().withRateDto(ratesDto)

        rateDataRepository.refreshRates(Rate.Currency.BGN.name).test().run {
            assertNoErrors()
            assertComplete()
            assertEquals(1, values().size)
            assertEquals(2, values()[0].size)
            assertRate(values()[0].first())
        }
    }

    @Test
    fun `convertAmount - should successfully converts amount`() {
        val cachedRates = hashMapOf(
            Rate.Currency.BGN.name to "1.9616",
            Rate.Currency.AUD.name to "1.6212"
        )
        val rateOne = createRate(
            currencyDisplayName = Rate.Currency.BGN.name,
            currency = Rate.Currency.BGN,
            amount = "1.9616"
        )
        val rateTwo = createRate(
            currencyDisplayName = Rate.Currency.AUD.name,
            currency = Rate.Currency.AUD,
            amount = "1.6212"
        )

        val rates = rateDataRepository.convertAmount(
            baseCurrencyCode = Rate.Currency.BGN.name,
            rates = listOf(rateOne, rateTwo),
            cachedRates = cachedRates,
            amount = 100.00
        )
        assertEquals(2, rates.size)
        val rate = rates.first()
        assertEquals(Rate.Currency.BGN.name, rate.currency.name)
        assertEquals("100.0", rate.amount)
        val convertedRate = rates[1]
        assertEquals(Rate.Currency.AUD.name, convertedRate.currency.name)
        assertEquals("162.12", convertedRate.amount)
    }

    private fun assertRate(rate: Rate) {
        with(rate) {
            assertTrue(amount != "0.0")
            assertTrue(currencyDisplayName.isNotEmpty())
        }
    }

    private inner class ArrangeBuilder {

        fun withRateDto(ratesDto: RatesDto) = apply {

            whenever(remote.refreshRates(any())).thenReturn(Single.just(Response.success(ratesDto)))
        }
    }
}