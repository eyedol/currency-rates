package com.addhen.currency.com.addhen.currency.data.api

import com.addhen.currency.data.api.RatesDto

object RatesDtoTestUtil {

    fun createRateDto(rates: Map<String, Double>): RatesDto {
        return RatesDto(rates)
    }
}