/*
 * MIT License
 *
 * Copyright (c) 2017 - 2019 Henry Addo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.addhen.currency.di.module

import com.addhen.currency.AppRxSchedulers
import com.addhen.currency.AppUtilities
import com.addhen.currency.BuildConfig
import com.addhen.currency.CurrencyApp
import com.addhen.currency.RxSchedulers
import com.addhen.currency.TimberUtility
import com.addhen.currency.data.AppRateHandler
import com.addhen.currency.data.RateHandler
import com.addhen.currency.data.api.ApiService
import com.addhen.currency.data.repository.rate.RateDataRepository
import com.addhen.currency.data.repository.rate.RateRepository
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import okhttp3.ConnectionPool
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Dagger module generally available for the entire lifecycle of the app.
 */

@Module
internal object AppModule {

    private const val BASE_URL = "https://revolut.duckdns.org/"
    private const val HTTP_TIMEOUT = 15L

    @Singleton
    @Provides
    @JvmStatic
    fun provideAppContext(app: CurrencyApp) = app.applicationContext

    @Provides
    @JvmStatic
    fun provideAppUtilities(timberUtility: TimberUtility
    ): AppUtilities = AppUtilities(timberUtility)

    @Provides
    @JvmStatic
    fun provideRxSchedulers(): RxSchedulers = AppRxSchedulers()

    @Singleton
    @Provides
    @JvmStatic
    fun provideMoshi(): Moshi {
        return Moshi.Builder().build()
    }

    @Singleton
    @Provides
    @JvmStatic
    fun provideRetrofit(
        okHttpClient: OkHttpClient,
        moshi: Moshi
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()
    }

    @Singleton
    @Provides
    @JvmStatic
    fun provideLoggingInterceptor(): HttpLoggingInterceptor {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor.Level.BODY
        } else {
            HttpLoggingInterceptor.Level.BASIC
        }
        return loggingInterceptor
    }

    @Singleton
    @Provides
    @JvmStatic
    fun provideOkHttpClient(loggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
        val okHttpClientBuilder = OkHttpClient.Builder()
        initOkHttpBuilder(okHttpClientBuilder, loggingInterceptor)
        return okHttpClientBuilder.build()
    }

    @Provides
    @Singleton
    @JvmStatic
    fun provideRatesRepository(
        searchArtistRepository: RateDataRepository
    ): RateRepository = searchArtistRepository

    @Provides
    @Singleton
    @JvmStatic
    fun provideRatesApi(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }

    @Provides
    @Singleton
    @JvmStatic
    fun provideRateHandler(): RateHandler {
        return AppRateHandler()
    }

    private fun initOkHttpBuilder(
        okHttpClientBuilder: OkHttpClient.Builder,
        loggingInterceptor: HttpLoggingInterceptor
    ) {
        okHttpClientBuilder.connectTimeout(HTTP_TIMEOUT, TimeUnit.SECONDS)
        okHttpClientBuilder.writeTimeout(HTTP_TIMEOUT, TimeUnit.SECONDS)
        okHttpClientBuilder.readTimeout(HTTP_TIMEOUT, TimeUnit.SECONDS)
        okHttpClientBuilder.addInterceptor(loggingInterceptor)
        okHttpClientBuilder.connectionPool(ConnectionPool(0, 1, TimeUnit.NANOSECONDS))
    }
}
