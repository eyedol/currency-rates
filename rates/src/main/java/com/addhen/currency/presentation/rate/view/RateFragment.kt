/*
 * MIT License
 *
 * Copyright (c) 2017 - 2019 Henry Addo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.addhen.currency.presentation.rate.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.addhen.currency.R
import com.addhen.currency.data.model.Rate
import com.addhen.currency.databinding.RateFragmentBinding
import com.addhen.currency.presentation.base.view.BaseFragment
import com.addhen.currency.presentation.base.view.BaseRecyclerAdapter
import com.google.android.material.snackbar.Snackbar

class RateFragment : BaseFragment<RateViewModel, RateFragmentBinding>(
    clazz = RateViewModel::class.java
) {

    private val rateAdapter: RateAdapter = RateAdapter()
    private val linearLayoutManager by lazy {
        LinearLayoutManager(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        binding = RateFragmentBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        observeViewStateChanges()
        observeViewSideEffectChanges()
    }

    private fun initView() {
        binding.rateRecycler.apply {
            layoutManager = linearLayoutManager
            adapter = rateAdapter
            itemAnimator = DefaultItemAnimator()
        }
        rateAdapter.onClickListener = object : BaseRecyclerAdapter.OnClickListener {

            override fun onClick(position: Int) {
                viewModel.onAction(RateAction.StopRefresh)
                rateAdapter.updateItemPosition(position)
                binding.rateRecycler.smoothScrollToPosition(0)
            }
        }
        rateAdapter.onAmountChangedListener = object : RateAdapter.OnAmountChangedListener {

            override fun onAmountChanged(baseCurrencyCode: String, amount: Double, rates: List<Rate>) {
                viewModel.onAction(RateAction.Convert(baseCurrencyCode, amount, rates))
            }

            override fun stopTicker() {
                viewModel.onAction(RateAction.StopRefresh)
            }
        }
    }

    private fun observeViewStateChanges() {
        viewModel.viewState.observe(viewLifecycleOwner, Observer {
            rateAdapter.reset(it.rates)
            binding.rateProgressBar.isVisible = it.isLoading
            binding.rateEmptyStateText.isVisible = it.isEmptyViewShown
            if (it.isEmptyViewShown) rateAdapter.reset(emptyList())
        })
    }

    private fun observeViewSideEffectChanges() {
        viewModel.viewEffect.observe(viewLifecycleOwner, Observer {
            when (it) {
                RateViewEffect.Error -> {
                    showRetrySnackMessage()
                }
            }
        })
    }

    private fun showRetrySnackMessage() {
        Snackbar.make(binding.rootView, R.string.rate_refresh_failed, Snackbar.LENGTH_LONG)
            .setAction(R.string.retry) {
                viewModel.onAction(RateAction.Initialize())
            }.show()
    }
}
