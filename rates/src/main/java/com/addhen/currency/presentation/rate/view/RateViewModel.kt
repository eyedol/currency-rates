/*
 * MIT License
 *
 * Copyright (c) 2017 - 2019 Henry Addo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.addhen.currency.presentation.rate.view

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.addhen.currency.RxSchedulers
import com.addhen.currency.data.model.Rate
import com.addhen.currency.data.repository.rate.RateRepository
import com.addhen.currency.presentation.base.view.BaseViewModel
import com.addhen.currency.presentation.base.view.state.Action
import com.addhen.currency.presentation.base.view.state.Reducer
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class RateViewModel @Inject constructor(
    private val schedulers: RxSchedulers,
    private val repository: RateRepository
) : BaseViewModel(), Reducer<RateState> {

    val viewState: LiveData<RateState>
        get() = mutableViewState
    val viewEffect: LiveData<RateViewEffect>
        get() = mutableViewEffect

    private lateinit var tickerDisposable: Disposable

    private val mutableViewState = MutableLiveData<RateState>()
    private val mutableViewEffect = MutableLiveData<RateViewEffect>()
    private val cachedRates = hashMapOf<String, String>()

    private var currentViewState = RateState()
        set(value) {
            field = value
            mutableViewState.postValue(value)
        }

    init {
        onAction(RateAction.Initialize())
    }

    override fun onAction(action: Action) {
        when (action) {
            is RateAction.Convert -> {
                convertAmount(
                    baseCurrencyCode = action.baseCurrencyCode,
                    amount = action.amount,
                    rates = action.rates
                )
            }
            is RateAction.Initialize -> {
                refreshRates(action.baseCurrencyCode)
            }
            is RateAction.StopRefresh -> {
                stopScheduler()
            }
            else -> {
                currentViewState
            }
        }
    }

    private fun stopScheduler() {
        if (::tickerDisposable.isInitialized) {
            tickerDisposable.dispose()
        }
    }

    private fun refreshRates(baseCurrencyCode: String) {
        tickerDisposable = Observable.interval(0, 1, TimeUnit.SECONDS, schedulers.disk())
            .flatMapSingle { repository.refreshRates(baseCurrencyCode) }
            .observeOn(schedulers.main())
            .retry(3)
            .distinct()
            .doOnSubscribe {
                currentViewState = currentViewState.copy(
                    isLoading = true,
                    isEmptyViewShown = false
                )
            }
            .subscribe(this::onSuccess, this::onError)
        compositeDisposable.add(tickerDisposable)
    }

    private fun convertAmount(baseCurrencyCode: String, amount: Double = 0.0, rates: List<Rate>) {
        val rateLocal = repository.convertAmount(baseCurrencyCode, amount, rates, cachedRates)
        currentViewState = currentViewState.copy(
            isLoading = false,
            isEmptyViewShown = rateLocal.isEmpty(),
            rates = rateLocal
        )
    }

    private fun onSuccess(rates: List<Rate>) {
        cachedRates.clear()
        cachedRates.putAll(rates.map { it.currency.name to it.amount })
        currentViewState = currentViewState.copy(
            isLoading = false,
            isEmptyViewShown = rates.isEmpty(),
            rates = rates
        )
    }

    private fun onError(throwable: Throwable) {
        Timber.e(throwable)
        currentViewState = currentViewState.copy(
            isLoading = false,
            isEmptyViewShown = true,
            rates = emptyList()
        )
        mutableViewEffect.postValue(RateViewEffect.Error)
    }
}
