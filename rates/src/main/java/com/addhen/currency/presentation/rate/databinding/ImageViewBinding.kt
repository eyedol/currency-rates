package com.addhen.currency.presentation.rate.databinding

import android.widget.ImageView
import androidx.databinding.BindingAdapter

@BindingAdapter(value = ["imageResId"])
fun imageResId(imageView: ImageView, resId: Int) {
    imageView.setImageResource(resId)
}