/*
 * MIT License
 *
 * Copyright (c) 2017 - 2019 Henry Addo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.addhen.currency.presentation.rate.view

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import com.addhen.currency.R
import com.addhen.currency.data.model.Rate
import com.addhen.currency.databinding.RateItemBinding
import com.addhen.currency.presentation.base.extension.hideKeyboard
import com.addhen.currency.presentation.base.extension.showKeyboard
import com.addhen.currency.presentation.base.view.BaseBindingHolder
import com.addhen.currency.presentation.base.view.BaseRecyclerAdapter
import kotlinx.android.synthetic.main.rate_item.view.*
import java.util.Collections
import java.util.Timer
import java.util.TimerTask

class RateAdapter : BaseRecyclerAdapter<Rate, BaseBindingHolder<RateItemBinding>>() {

    var onAmountChangedListener: OnAmountChangedListener? = null

    var isEditTextFocused: Boolean = false

    var timer: Timer? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseBindingHolder<RateItemBinding> {
        return RateViewHolder(parent, R.layout.rate_item).apply {
            itemView.setOnClickListener {
                onClickListener?.onClick(adapterPosition)
                itemView.currency_amount.clearFocus()
                itemView.context.hideKeyboard(view)
            }
        }
    }

    override fun onBindViewHolder(holder: BaseBindingHolder<RateItemBinding>, position: Int) {
        val itemBinding = holder.binding
        bindCurrencyAmount(itemBinding, position)
        itemBinding.rate = getItem(position)
        itemBinding.executePendingBindings()
    }

    private fun bindCurrencyAmount(itemBinding: RateItemBinding, position: Int) {
        itemBinding.currencyAmount.setOnClickListener { v ->
            v.isFocusableInTouchMode = true
            v.requestFocus()
            onAmountChangedListener?.stopTicker()
        }
        itemBinding.currencyAmount.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            isEditTextFocused = hasFocus
            if (hasFocus) {
                itemBinding.currencyAmount.context.showKeyboard(itemBinding.currencyAmount)
                itemBinding.currencyAmount.setSelection(itemBinding.currencyAmount.text.length)
                onAmountChangedListener?.stopTicker()
            } else {
                v.clearFocus()
                itemBinding.currencyAmount.isFocusableInTouchMode = false
                itemBinding.currencyAmount.context.hideKeyboard(v)
            }
            if (position != 0 && hasFocus) {
                updateItemPosition(position)
            }
        }
        itemBinding.currencyAmount.currency_amount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                timer = Timer()
                timer?.schedule(object : TimerTask() {
                    override fun run() {
                        if (position == 0 && isEditTextFocused) {
                            onAmountChangedListener?.stopTicker()
                            if (s.toString().isNotEmpty()) {
                                onAmountChangedListener?.onAmountChanged(
                                    baseCurrencyCode = item.first().currency.name,
                                    amount = s.toString().toDouble(),
                                    rates = item
                                )
                            }
                        }
                    }
                }, 600)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                // No op
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                // No op
                timer?.cancel()
            }
        })
    }

    fun updateItemPosition(position: Int) {
        Collections.swap(item, position, 0)
        notifyItemRangeChanged(0, item.size)
        notifyItemMoved(position, 0)
    }

    class RateViewHolder(val view: ViewGroup, @LayoutRes layoutResId: Int) :
        BaseBindingHolder<RateItemBinding>(view.context, view, layoutResId)

    interface OnAmountChangedListener {

        fun onAmountChanged(baseCurrencyCode: String, amount: Double, rates: List<Rate>)

        fun stopTicker()
    }
}
