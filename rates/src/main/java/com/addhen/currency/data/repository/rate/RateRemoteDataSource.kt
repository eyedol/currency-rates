package com.addhen.currency.data.repository.rate

import com.addhen.currency.data.api.ApiService
import com.addhen.currency.data.api.RatesDto
import io.reactivex.Single
import retrofit2.Response
import javax.inject.Inject

class RateRemoteDataSource @Inject constructor(private val apiService: ApiService) {

    fun refreshRates(currencyCode: String): Single<Response<RatesDto>> {
        return apiService.getCurrencyData(currencyCode)
    }
}