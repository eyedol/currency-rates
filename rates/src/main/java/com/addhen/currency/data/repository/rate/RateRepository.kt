package com.addhen.currency.data.repository.rate

import com.addhen.currency.data.model.Rate
import io.reactivex.Single

interface RateRepository {

    fun refreshRates(baseCurrencyCode: String): Single<List<Rate>>

    fun convertAmount(
        baseCurrencyCode: String,
        amount: Double,
        rates: List<Rate>,
        cachedRates: Map<String, String>
    ): List<Rate>
}