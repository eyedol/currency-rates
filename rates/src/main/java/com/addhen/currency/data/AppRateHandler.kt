package com.addhen.currency.data

import com.addhen.currency.data.model.Rate
import timber.log.Timber
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.NumberFormat
import java.util.Currency
import java.util.Locale
import javax.inject.Inject

class AppRateHandler @Inject constructor() : RateHandler {

    override fun getDisplayName(currencyCode: String, locale: Locale): String {
        return try {
            Currency.getInstance(currencyCode).getDisplayName(locale)
        } catch (exception: Exception) {
            Timber.d(exception, ": $currencyCode")
            Timber.e(exception)
            currencyCode
        }
    }

    override fun getFlag(currencyCode: String): Rate.Currency {
        return when (currencyCode) {
            Rate.Currency.AUD.name -> Rate.Currency.AUD
            Rate.Currency.BGN.name -> Rate.Currency.BGN
            Rate.Currency.BRL.name -> Rate.Currency.BRL
            Rate.Currency.CAD.name -> Rate.Currency.CAD
            Rate.Currency.CHF.name -> Rate.Currency.CHF
            Rate.Currency.CNY.name -> Rate.Currency.CNY
            Rate.Currency.CZK.name -> Rate.Currency.CZK
            Rate.Currency.DKK.name -> Rate.Currency.DKK
            Rate.Currency.EUR.name -> Rate.Currency.EUR
            Rate.Currency.GBP.name -> Rate.Currency.GBP
            Rate.Currency.HKD.name -> Rate.Currency.HKD
            Rate.Currency.HRK.name -> Rate.Currency.HRK
            Rate.Currency.HUF.name -> Rate.Currency.HUF
            Rate.Currency.IDR.name -> Rate.Currency.IDR
            Rate.Currency.ILS.name -> Rate.Currency.ILS
            Rate.Currency.INR.name -> Rate.Currency.INR
            Rate.Currency.ISK.name -> Rate.Currency.ISK
            Rate.Currency.JPY.name -> Rate.Currency.JPY
            Rate.Currency.KRW.name -> Rate.Currency.KRW
            Rate.Currency.MXN.name -> Rate.Currency.MXN
            Rate.Currency.MYR.name -> Rate.Currency.MYR
            Rate.Currency.NOK.name -> Rate.Currency.NOK
            Rate.Currency.NZD.name -> Rate.Currency.NZD
            Rate.Currency.PHP.name -> Rate.Currency.PHP
            Rate.Currency.PLN.name -> Rate.Currency.PLN
            Rate.Currency.RON.name -> Rate.Currency.RON
            Rate.Currency.RUB.name -> Rate.Currency.RUB
            Rate.Currency.SEK.name -> Rate.Currency.SEK
            Rate.Currency.SGD.name -> Rate.Currency.SGD
            Rate.Currency.THB.name -> Rate.Currency.THB
            Rate.Currency.TRY.name -> Rate.Currency.TRY
            Rate.Currency.USD.name -> Rate.Currency.USD
            Rate.Currency.ZAR.name -> Rate.Currency.ZAR
            else -> Rate.Currency.NA
        }
    }

    override fun formatCurrency(amount: BigDecimal, locale: Locale, fractionDigits: Int): String {
        val currencyFormant = NumberFormat.getNumberInstance(locale).apply {
            maximumFractionDigits = fractionDigits
            roundingMode = RoundingMode.FLOOR
        }
        return currencyFormant.format(amount)
    }

    override fun getFractionDigits(currencyCode: String): Int {
        return try {
            Currency.getInstance(currencyCode).defaultFractionDigits
        } catch (exception: Exception) {
            Timber.e(exception)
            2
        }
    }

    override fun getConversionRate(rate: String, amount: String): BigDecimal {
        return try {
            val bigRate = BigDecimal(rate.replace(",", ""))
            val bigAmount = BigDecimal(amount.replace(",", ""))
            bigRate.multiply(bigAmount)
        } catch (exception: Exception) {
            Timber.e(exception)
            BigDecimal("0")
        }
    }
}