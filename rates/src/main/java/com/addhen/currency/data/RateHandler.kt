package com.addhen.currency.data

import com.addhen.currency.data.model.Rate
import java.math.BigDecimal
import java.util.Locale

interface RateHandler {

    fun getDisplayName(currencyCode: String, locale: Locale = Locale.getDefault()): String

    fun getFlag(currencyCode: String): Rate.Currency

    fun formatCurrency(amount: BigDecimal, locale: Locale = Locale.getDefault(), fractionDigits: Int = 2): String

    fun getFractionDigits(currencyCode: String): Int

    fun getConversionRate(rate: String, amount: String): BigDecimal
}