package com.addhen.currency.data.model

import androidx.annotation.DrawableRes
import com.addhen.currency.R

data class Rate(val currencyDisplayName: String, val currency: Currency, val amount: String = "0") {

    enum class Currency(@DrawableRes val icon: Int) {
        AUD(icon = R.drawable.ic_australia_flag),
        BGN(icon = R.drawable.ic_bulgaria_flag),
        BRL(icon = R.drawable.ic_brazil_flag),
        CAD(icon = R.drawable.ic_canada_flag),
        CHF(icon = R.drawable.ic_switzerland_flag),
        CNY(icon = R.drawable.ic_china_flag),
        CZK(icon = R.drawable.ic_czech_republic_flag),
        DKK(icon = R.drawable.ic_denmark_flag),
        EUR(icon = R.drawable.ic_european_union_flag),
        GBP(icon = R.drawable.ic_united_kingdom_flag),
        HKD(icon = R.drawable.ic_hong_kong_flag),
        HRK(icon = R.drawable.ic_croatia_flag),
        HUF(icon = R.drawable.ic_hungary_flag),
        IDR(icon = R.drawable.ic_indonesia_flag),
        ILS(icon = R.drawable.ic_israel_flag),
        INR(icon = R.drawable.ic_india_flag),
        ISK(icon = R.drawable.ic_iceland_flag),
        JPY(icon = R.drawable.ic_japan_flag),
        KRW(icon = R.drawable.ic_south_korea_flag),
        MXN(icon = R.drawable.ic_mexico_flag),
        MYR(icon = R.drawable.ic_malaysia_flag),
        NOK(icon = R.drawable.ic_norway_flag),
        NZD(icon = R.drawable.ic_new_zealand_flag),
        PHP(icon = R.drawable.ic_philippines_flag),
        PLN(icon = R.drawable.ic_republic_of_poland_flag),
        RON(icon = R.drawable.ic_romania_flag),
        RUB(icon = R.drawable.ic_russia_flag),
        SEK(icon = R.drawable.ic_sweden_flag),
        SGD(icon = R.drawable.ic_singapore_flag),
        THB(icon = R.drawable.ic_thailand_flag),
        TRY(icon = R.drawable.ic_turkey_flag),
        USD(icon = R.drawable.ic_united_states_of_america_flag),
        ZAR(icon = R.drawable.ic_south_africa_flag),
        NA(icon = R.drawable.ic_na_flag),
    }
}