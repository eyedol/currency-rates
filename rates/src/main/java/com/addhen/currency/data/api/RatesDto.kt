package com.addhen.currency.data.api

data class RatesDto(val rates: Map<String, Double>)