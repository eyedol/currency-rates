package com.addhen.currency.data.repository.rate

import com.addhen.currency.data.RateHandler
import com.addhen.currency.data.api.RatesDto
import com.addhen.currency.data.model.Rate
import io.reactivex.Single
import java.math.BigDecimal
import javax.inject.Inject

class RateDataRepository @Inject constructor(
    private val remote: RateRemoteDataSource,
    private val rate: RateHandler
) : RateRepository {

    override fun convertAmount(
        baseCurrencyCode: String,
        amount: Double,
        rates: List<Rate>,
        cachedRates: Map<String, String>
    ): List<Rate> {
        // Please don't do this at work. A quick way to avoid caching because of time
        val rate = rates.first { it.currency.name == baseCurrencyCode }
        val m = mutableListOf<Rate>()
        rates.mapTo(m, {
            val cachedRate = cachedRates[it.currency.name]
            val bigRate = BigDecimal(cachedRate?.replace(",", ""))
            val bigAmount = BigDecimal(amount.toString().replace(", ", ""))
            toRate(currencyCode = it.currency.name, amount = bigAmount.toDouble(), rate = bigRate.toDouble())
        })
        return m.mapIndexed { _, r -> if (r.currency.name == rate.currency.name) rate.copy(amount = amount.toString()) else r }
    }

    override fun refreshRates(baseCurrencyCode: String): Single<List<Rate>> {
        return remote.refreshRates(baseCurrencyCode).map { toRates(it.body()!!) }
    }

    private fun toRates(ratesDto: RatesDto): List<Rate> {
        return ratesDto.rates.map { toRate(it.key, it.value) }
    }

    private fun toRate(currencyCode: String, amount: Double): Rate {
        return toRate(currencyCode, amount, 0.0)
    }

    private fun toRate(currencyCode: String, amount: Double, rate: Double): Rate {
        val r = if (rate > 0.0) rate else amount
        val rateLocal = this.rate.getConversionRate(
            rate = r.toString(),
            amount = amount.toString()
        )
        val flag = this.rate.getFlag(currencyCode)
        val fractionDigits = this.rate.getFractionDigits(currencyCode)
        val value = this.rate.formatCurrency(
            amount = rateLocal,
            fractionDigits = fractionDigits
        )
        val displayName = this.rate.getDisplayName(currencyCode)
        return Rate(currencyDisplayName = displayName, currency = flag, amount = value)
    }
}