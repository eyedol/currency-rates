<?xml version="1.0" encoding="utf-8"?><!--
  ~ MIT License
  ~
  ~ Copyright (c) 2017 - 2019 Henry Addo
  ~
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy
  ~ of this software and associated documentation files (the "Software"), to deal
  ~ in the Software without restriction, including without limitation the rights
  ~ to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  ~ copies of the Software, and to permit persons to whom the Software is
  ~ furnished to do so, subject to the following conditions:
  ~
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  ~ FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  ~ AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  ~ LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  ~ OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  ~ SOFTWARE.
  -->

<layout xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:app="http://schemas.android.com/apk/res-auto"
        xmlns:tools="http://schemas.android.com/tools">

    <data>

        <variable
                name="rate"
                type="com.addhen.currency.data.model.Rate" />
    </data>

    <androidx.constraintlayout.widget.ConstraintLayout
            android:id="@+id/rootView"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:clickable="true"
            android:focusable="true"
            android:focusableInTouchMode="false">

        <ImageView
                android:id="@+id/currency_image"
                android:layout_width="45dp"
                android:layout_height="45dp"
                android:layout_marginStart="@dimen/space_16dp"
                android:layout_marginTop="@dimen/space_8dp"
                android:layout_marginBottom="@dimen/space_8dp"
                android:contentDescription="@string/currency_name"
                app:imageResId="@{rate.currency.icon}"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toTopOf="parent"
                tools:src="@tools:sample/avatars" />

        <TextView
                android:id="@+id/currency_name"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:gravity="start|center_vertical"
                android:paddingStart="@dimen/space_16dp"
                android:paddingEnd="@dimen/space_16dp"
                android:text="@{rate.currencyDisplayName}"
                android:textAppearance="@style/TextSubtitle1"
                android:textColor="@color/grey300"
                app:layout_constrainedWidth="true"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintEnd_toStartOf="@id/currency_amount"
                app:layout_constraintHorizontal_bias="0"
                app:layout_constraintStart_toEndOf="@id/currency_image"
                app:layout_constraintTop_toBottomOf="@id/currency_code"
                app:layout_constraintWidth_min="@dimen/min_width"
                app:layout_goneMarginTop="@dimen/space_16dp"
                tools:text="@sample/rates.json/data/name" />

        <TextView
                android:id="@+id/currency_code"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:gravity="start|center_vertical"
                android:paddingStart="@dimen/space_16dp"
                android:paddingEnd="@dimen/space_16dp"
                android:text="@{rate.currency.name()}"
                android:textAppearance="@style/TextSubtitle1"
                android:textStyle="bold"
                app:layout_constrainedWidth="true"
                app:layout_constraintBottom_toTopOf="@id/currency_name"
                app:layout_constraintEnd_toStartOf="@id/currency_amount"
                app:layout_constraintHorizontal_bias="0"
                app:layout_constraintStart_toEndOf="@+id/currency_image"
                app:layout_constraintTop_toTopOf="parent"
                app:layout_constraintWidth_min="@dimen/min_width"
                app:layout_goneMarginBottom="@dimen/space_16dp"
                tools:text="@sample/rates.json/data/code" />

        <androidx.constraintlayout.widget.Barrier
                android:id="@+id/text_barrier"
                android:layout_width="0dp"
                android:layout_height="0dp"
                app:barrierDirection="end"
                app:constraint_referenced_ids="currency_name,currency_code" />

        <EditText
                android:id="@+id/currency_amount"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginEnd="@dimen/space_16dp"
                android:layout_marginBottom="20dp"
                android:ems="10"
                android:focusableInTouchMode="false"
                android:hint="0"
                android:inputType="numberDecimal"
                android:maxWidth="250dp"
                android:maxLines="1"
                android:minWidth="100dp"
                android:text="@{rate.amount}"
                android:textAlignment="viewEnd"
                android:textAppearance="@style/TextSubtitle1"
                android:textColorHint="@color/grey300"
                android:textStyle="bold"
                app:backgroundTint="@color/grey300"
                app:layout_constrainedWidth="true"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintHorizontal_bias="1"
                app:layout_constraintStart_toEndOf="@+id/text_barrier"
                app:layout_constraintTop_toTopOf="parent"
                app:layout_constraintWidth_min="@dimen/min_width"
                tools:text="1.6179" />
    </androidx.constraintlayout.widget.ConstraintLayout>
</layout>

