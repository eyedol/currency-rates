plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-kapt")
    id("kotlin-android-extensions")
    id("androidx.navigation.safeargs")
}

android {
    compileSdkVersion(Project.COMPILE_SDK)
    dataBinding.isEnabled = true
    defaultConfig {
        applicationId = Project.APPLICATION_ID
        minSdkVersion(Project.MIN_SDK)
        targetSdkVersion(Project.TARGET_SDK)
        versionCode = Project.VERSION_CODE
        versionName = Project.VERSION_NAME
        setProperty("archivesBaseName", "${project.name}-$versionName")
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables.useSupportLibrary = true
    }

    dexOptions {
        preDexLibraries = "true" != System.getenv("CI")
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            isShrinkResources = false
            proguardFile(getDefaultProguardFile("proguard-android.txt"))
            proguardFile(file("proguard-rules.pro"))
        }
    }

    lintOptions {
        setLintConfig(file("${project.projectDir}/lint.xml"))
        disable("AppCompatResource", "ObsoleteLintCustomCheck")
        textReport = true
        textOutput("stdout")
    }

    compileOptions {
        setSourceCompatibility(Project.SOURCE_COMPATIBILITY)
        setTargetCompatibility(Project.SOURCE_COMPATIBILITY)
    }

    packagingOptions {
        exclude("META-INF/atomicfu.kotlin_module")
    }
}

dependencies {
    implementation(Dependencies.Kotlin.stdlibJvm)
    implementation(Dependencies.RxJava2.core)
    implementation(Dependencies.RxJava2.android)
    implementation(Dependencies.RxJava2.kotlin)
    // AndroidX
    implementation(Dependencies.AndroidX.appCompat)
    implementation(Dependencies.AndroidX.design)
    implementation(Dependencies.AndroidX.recyclerView)
    implementation(Dependencies.AndroidX.constraint)
    // AndroidX Navigation
    implementation(Dependencies.AndroidX.Navigation.runtime)
    implementation(Dependencies.AndroidX.Navigation.ui)
    implementation(Dependencies.AndroidX.Navigation.runtimeKtx)
    implementation(Dependencies.AndroidX.Navigation.fragmentKtx)
    implementation(Dependencies.AndroidX.Navigation.uiKtx)
    // AndroidX coreKtx
    implementation(Dependencies.AndroidX.coreKtx)
    // Architecture components lifecycle
    implementation(Dependencies.AndroidX.Lifecycle.runtime)
    implementation(Dependencies.AndroidX.Lifecycle.liveData)
    implementation(Dependencies.AndroidX.Lifecycle.extensions)
    implementation(Dependencies.AndroidX.Lifecycle.viewModelKtx)
    // Utility
    implementation(Dependencies.Dagger.core)
    implementation(Dependencies.Dagger.android)
    implementation(Dependencies.Dagger.support)
    implementation(Dependencies.timber)
    implementation(Dependencies.Square.Retrofit.core) {
        exclude(module = "okhttp")
    }
    implementation(Dependencies.Square.Retrofit.moshi)
    implementation(Dependencies.Square.moshi)
    implementation(Dependencies.Square.Retrofit.rxJava)
    implementation(Dependencies.Square.OkHttp.core)
    implementation(Dependencies.Square.OkHttp.loggingInterceptor)
    implementation(Dependencies.Dagger.core)
    implementation(Dependencies.timber)

    kapt(Dependencies.Databinding.compiler)
    kapt(Dependencies.AndroidX.Lifecycle.compiler)
    kapt(Dependencies.Databinding.compiler)
    kapt(Dependencies.Dagger.compiler)
    kapt(Dependencies.Dagger.processor)

    testImplementation(Dependencies.Test.junit)
    testImplementation(Dependencies.Test.mockitoKotlin)
    testImplementation(Dependencies.Test.mockitoInline)
    testImplementation(Dependencies.Test.archCoreTesting)
}
